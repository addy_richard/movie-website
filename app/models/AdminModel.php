<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/13/2018
 * Time: 2:48 PM
 */

class AdminModel
{
    private $db;
    private $admin;

    public function __construct()
    {
        $this->db = new Database();
    }


    public function login($email , $password){
        if($this->findAdminByEmail($email)){
            $admin_pass = $this->admin->password;

            if (password_verify($password,$admin_pass)){
                return $this->admin;
            }else{
                return false;
            }
        }
    }

    public function findAdminByEmail($email){
        $query = 'SELECT * FROM admin WHERE email = :email';

        $this->db->query($query);
        $this->db->bind(':email',$email);
        $this->admin = $this->db->singleRow();

        if ($this->db->rowCount() > 0){
            return true;
        }else{
            return false;
        }

    }

    public function registerAdmin($data){
        $query = 'INSERT INTO admin (`name`, `email`, `password`) VALUES (:name , :email , :password)';

        $this->db->query($query);
        $this->db->bind(':name',$data['name']);
        $this->db->bind(':email',$data['email']);
        $this->db->bind(':password',password_hash($data['password'],PASSWORD_ARGON2I));
        if($this->db->execute()){
            return true;
        }else{
            return false;
        }
    }

    public function getAllMoviesAndSeries(){
        $sql = 'SELECT * FROM movies';
        $this->db->query($sql);
        $allMovies = $this->db->allRows();
        $this->count = $this->db->rowCount();

        return $allMovies;
    }

    public function getMoviesCount(){
        $sql = 'SELECT * FROM movies';
        $this->db->query($sql);
        $this->db->execute();
        $movieCount = $this->db->rowCount();

        return $movieCount;
    }

    public function getAllAdmins(){
        $sql = 'SELECT * FROM admin';
        $this->db->query($sql);
        $allAdmins = $this->db->allRows();

        return $allAdmins;
    }

    public function getAdminCount(){
        $sql = 'SELECT * FROM admin';
        $this->db->query($sql);
        $this->db->execute();
        $adminCount = $this->db->rowCount();

        return $adminCount;
    }

    public function getAllNews(){

    }

    public function getNewsCount(){

    }

}