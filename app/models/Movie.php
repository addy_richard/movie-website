<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/12/2018
 * Time: 7:52 PM
 */

class Movie
{
    private $db;
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getMovies(){
        $query = 'SELECT * FROM movies WHERE type = :type';

        $this->db->query($query);
        $this->db->bind(':type','movie');
        $movies = $this->db->allRows();

        return $movies;
    }

    public function getSeries(){
        $query = 'SELECT * FROM movies WHERE type = :type';

        $this->db->query($query);
        $this->db->bind(':type','series');
        $series = $this->db->allRows();

        return $series;
    }

    public function getAnimations(){
        $query = 'SELECT * FROM movies WHERE type = :type';

        $this->db->query($query);
        $this->db->bind(':type','animation');
        $animations = $this->db->allRows();

        return $animations;
    }

    public function getMovieByTitle($title){
        $query = 'SELECT * FROM movies WHERE title LIKE %:title%';
        $this->db->query($query);
        $this->db->bind(':title' , $title);
        $result = $this->db->allRows();

        if ($this->db->rowCount() > 0){
            return $result;
        }else{
            return false;
        }
    }

}