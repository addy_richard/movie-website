<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/7/2018
 * Time: 2:32 PM
 */

class Database
{
    private $host = DB_HOST;
    private $db_user = DB_USER;
    private $db_password = '';
    private $db_name = DB_NAME;

    private $connection;
    private $statement;

    public function __construct()
    {
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        $dsn = 'mysql:host='.$this->host.';dbname='.$this->db_name;

        try{
            $this->connection = new PDO($dsn,$this->db_user,$this->db_password,$options);
        }catch (PDOException $e){
            echo $e->getMessage();
        }

    }

    public function query($sql){
        $this->statement = $this->connection->prepare($sql);
    }

    public function bind($param,$value,$type = null){

        if (is_null($type)){
            switch (true){
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;

                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;

                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;

                default:
                    $type = PDO::PARAM_STR;
            }
        }

        $this->statement->bindValue($param,$value,$type);
    }

    public function execute(){
        return $this->statement->execute();
    }

    public function allRows(){
        $this->execute();

        return $this->statement->fetchAll(PDO::FETCH_OBJ);
    }

    public function singleRow(){
        $this->execute();

        return $this->statement->fetch(PDO::FETCH_OBJ);

    }

    public function rowCount(){
        return $this->statement->rowCount();
    }

}