<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 10/27/2018
 *
 * BASE CONTROLLER
 * Loads the models and views
 *
 */

    class Controller{
        public function model($model){
            //REQUIRE MODEL FILE

            require_once '../app/models/' .$model . '.php';

            //instantiate model

            return new $model;
        }

        //Load View

        public function view($view, $data = []){
            //Check for view file
            if (file_exists('../app/views/'. $view . '.php')){
                //REQUIRE VIEW FILE

                require_once '../app/views/' . $view . '.php';
            }else{
                die("View does not exist");
            }
        }
    }