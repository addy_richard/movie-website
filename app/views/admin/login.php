<?php require APP_ROOT . '/views/admin/admin_header.php'; ?>

<div class="row">
    <div class="col-md-5 mx-auto">
        <div class="card card-body bg-light mt-5">

            <h4>Welcome to Admin Panel</h4>
            <p>Please fill in your credentials to log in</p>

            <form action="<?php echo URL_ROOT; ?>/admin/login" method="post">

                <div class="form-group">
                    <label for="email">Email: <sup>*</sup></label>
                    <input type="email" value="<?php echo $data['email']; ?>"
                           class="form-control form-control-lg  <?php echo (!empty($data['email_error'])) ? 'is-invalid' : ''; ?>"
                           placeholder="Email" name="email" id="email">
                    <span class="invalid-feedback"><?php echo $data['email_error']; ?></span>
                </div>

                <div class="form-group">
                    <label for="password">Password: <sup>*</sup></label>
                    <input type="password" value="<?php echo $data['password']; ?>"
                           class="form-control form-control-lg  <?php echo (!empty($data['password_error'])) ? 'is-invalid' : ''; ?>"
                           placeholder="Password" name="password" id="password">
                    <span class="invalid-feedback"><?php echo $data['password_error']; ?></span>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" class="btn btn-primary btn-block mt-3" value="Login">
                    </div>

                    <div class="col">
                        <a href="<?php echo URL_ROOT; ?>/admin/register" class="btn btn-light btn-block mt-3">Don't have an account? Register</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<?php require APP_ROOT . '/views/admin/admin_footer.php'; ?>
