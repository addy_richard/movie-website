<nav class="navbar navbar-expand-md navbar-dark bg-dark">

    <div class="container">
        <a href="<?php echo URL_ROOT; ?>" class="navbar-brand"><?php echo SITE_NAME; ?> - Admin Panel</a>

        <button class="navbar-toggler" data-toggle="collapse" data-target="#menu">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="menu">

            <?php if (isset($_SESSION['admin_id'])) : ?>

            <ul class="navbar-nav">
                <li class="nav-item px-2">
                    <a href="index.html" class="nav-link active">Dashboard</a>
                </li>

                <li class="nav-item px-2">
                    <a href="posts.html" class="nav-link">Movies/Series</a>
                </li>

                <li class="nav-item px-2">
                    <a href="categories.html" class="nav-link">News</a>
                </li>

                <li class="nav-item px-2">
                    <a href="users.html" class="nav-link">Admins</a>
                </li>

            </ul>

            <?php endif; ?>

            <ul class="navbar-nav ml-auto">
                <?php if (isset($_SESSION['admin_id'])) : ?>
                <li class="nav-item">
                    <a href="<?php echo URL_ROOT; ?>/admin/logout" class="nav-link">
                        <i class="fa fa-user-times"></i>  Logout
                    </a>
                </li>
                <?php endif; ?>
                <li class="nav-item mx-2">
                    <a href="<?php echo URL_ROOT; ?>" class="nav-link"><i class="fa fa-globe"></i>  Visit Website</a>
                </li>


            </ul>

        </div>
    </div>

</nav>