<?php require APP_ROOT . '/views/admin/admin_header.php'; ?>


<header id="main-header" class="py-2 bg-primary text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>
                    <i class="fa fa-tachometer-alt"></i> Dashboard
                </h1>
            </div>
        </div>
    </div>
</header>

<!-- ACTIONS -->

<section id="action" class="py-4 mb-4 bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3 my-2">
                <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addMovieModal">
                    <i class="fa fa-plus"></i> Add Movie/Series
                </a>
            </div>
            <div class="col-md-3 my-2">
                <a href="#" data-toggle="modal" data-target="#addNewsModal" class="btn btn-success btn-block">
                    <i class="fa fa-plus"></i> Add News
                </a>
            </div>
            <div class="col-md-3 my-2">
                <a href="#" data-toggle="modal" data-target="#addAdminModal" class="btn btn-warning btn-block">
                    <i class="fa fa-plus"></i> Add Admin
                </a>
            </div>
        </div>
    </div>
</section>

<!-- POSTS -->

<section id="posts">
    <div class="container">

        <div class="row">
            <div class="col-md-4 mb-4">
                <div class="card text-white bg-primary text-center mb-3">
                    <div class="card-body">
                        <h3>Movies & Series</h3>
                        <h1 class="display-4">
                            <i class="fa fa-pencil-alt"></i> <?php echo $data['movieCount']; ?>
                        </h1>
                        <a href="posts.html" class="btn btn-outline-light btn-sm">View</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mb-4">
                <div class="card text-white bg-success text-center mb-3">
                    <div class="card-body">
                        <h3>News</h3>
                        <h1 class="display-4">
                            <i class="fa fa-folder-open"></i> 6
                        </h1>
                        <a href="categories.html" class="btn btn-outline-light btn-sm">View</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mbb-4">
                <div class="card text-white bg-warning text-center mb-3">
                    <div class="card-body">
                        <h3>Admins</h3>
                        <h1 class="display-4">
                            <i class="fa fa-users"></i> <?php echo $data['adminCount']; ?>
                        </h1>
                        <a href="#" class="btn btn-outline-light btn-sm">View</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h4>Latest Movies and Tv-Series</h4>
                    </div>

                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Date Posted</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td scope="row">1</td>
                            <td>Post One</td>
                            <td>Web Development</td>
                            <td>December 15, 2018</td>
                            <td>
                                <a href="details.html" class="btn btn-outline-secondary">
                                    <i class="fa fa-angle-double-right"></i> Details
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td scope="row">2</td>
                            <td>Post Two</td>
                            <td>Web Development</td>
                            <td>December 15, 2018</td>
                            <td>
                                <a href="details.html" class="btn btn-outline-secondary">
                                    <i class="fa fa-angle-double-right"></i> Details
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td scope="row">3</td>
                            <td>Post Three</td>
                            <td>Web Development</td>
                            <td>December 15, 2018</td>
                            <td>
                                <a href="details.html" class="btn btn-outline-secondary">
                                    <i class="fa fa-angle-double-right"></i> Details
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td scope="row">4</td>
                            <td>Post Four</td>
                            <td>Web Development</td>
                            <td>December 15, 2018</td>
                            <td>
                                <a href="details.html" class="btn btn-outline-secondary">
                                    <i class="fa fa-angle-double-right"></i> Details
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td scope="row">5</td>
                            <td>Post Five</td>
                            <td>Web Development</td>
                            <td>December 15, 2018</td>
                            <td>
                                <a href="details.html" class="btn btn-outline-secondary">
                                    <i class="fa fa-angle-double-right"></i> Details
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>




<div id="addMovieModal" class="modal fade" role="dialog">

    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

        <div class="modal-content">

            <div class="modal-header bg-primary text-white">

                <h5 class="modal-title">Add Movie</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
            </div>
            <div class="modal-body">
                <form>

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" id="title" placeholder="Title" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="category">Category</label>
                        <select class="form-control" id="category">
                            <option>Web Developments</option>
                            <option>Tech Gadgets</option>
                            <option value="">Business</option>
                            <option>Health & Wellness</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="file">Image Upload</label>
                        <input type="file" id="file" class="form-control-file">
                        <span class="form-text text-muted">Max size 3mb</span>
                    </div>

                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea id="body" name="editor1" class="form-control"></textarea>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" data-dismiss="modal">Save Changes</button>
            </div>

        </div>
    </div>
</div>

<!-- CATEGORY MODAL -->

<div class="modal fade" id="addNewsModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-success text-white">
                <h5 class="modal-title">Add News</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label for="cat_title">Title</label>
                    <input type="text" placeholder="Title" class="form-control" id="cat_title">
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-success" data-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>

<!--  ADMIN MODAL -->
<div class="modal fade" id="addAdminModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning text-white">

                <h5 class="modal-title">Add Admin</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" placeholder="Name" class="form-control" id="name">
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" placeholder="Email" class="form-control" id="email">
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" placeholder="Password" class="form-control" id="password">
                </div>

                <div class="form-group">
                    <label for="confirm_password">Confirm Password</label>
                    <input type="password" placeholder="Confirm Password" class="form-control" id="confirm_password">
                </div>

            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-success" data-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>





<?php require APP_ROOT . '/views/admin/admin_footer.php'; ?>
