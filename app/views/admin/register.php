<?php require APP_ROOT . '/views/admin/admin_header.php'; ?>

<div class="row">
    <div class="col-sm-5 mx-auto">
        <div class="card card-body bg-light my-5">
            <h4>Welcome to Admin Panel</h4>
            <p>Please fill in the form to register</p>

            <form action="<?php echo URL_ROOT; ?>/admin/register" method="post">
                <div class="form-group">
                    <label for="name">Full Name: <sup>*</sup></label>

                    <input type="text" value="<?php echo $data['name']; ?>"
                           placeholder="Full name" name="name" id="name"
                           class="form-control form-control-lg <?php echo (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>">

                    <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>
                </div>

                <div class="form-group">
                    <label for="email">Email: <sup>*</sup></label>

                    <input type="email" value="<?php echo $data['email']; ?>"
                           placeholder="Email" id="email" name="email"
                           class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>">

                    <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
                </div>

                <div class="form-group">
                    <label for="password">Password: <sup>*</sup></label>

                    <input type="password" value="<?php echo $data['password']; ?>"
                           placeholder="Password" name="password" id="password"
                           class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>">

                    <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
                </div>

                <div class="form-group">
                    <label for="confirm_password">Confirm Password: <sup>*</sup></label>

                    <input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password"
                           value="<?php echo $data['confirm_password']; ?>"
                           class="form-control form-control-lg <?php echo (!empty($data['confirm_password_err'])) ? 'is-invalid' : ''; ?>">

                    <span class="invalid-feedback"><?php echo $data['confirm_password_err']; ?></span>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Register" name="submit" class="btn btn-success btn-block mt-3">
                    </div>

                    <div class="col">
                        <a href="<?php echo URL_ROOT?>/admin/login" class="btn btn-light btn-block mt-3">
                            Having an account? Login
                        </a>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>



<?php require APP_ROOT . '/views/admin/admin_footer.php'; ?>
