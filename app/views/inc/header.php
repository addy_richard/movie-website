<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo URL_ROOT; ?>/public/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Charm|Raleway" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo URL_ROOT; ?>/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo URL_ROOT; ?>/public/css/style.css">
    <title><?php echo SITE_NAME; ?></title>
</head>
<body>

<?php include APP_ROOT . '/views/inc/navbar.php'; ?>



