
<nav class="navbar navbar-expand-md navbar-dark bg-dark">

    <div class="container">
        <a href="<?php echo URL_ROOT; ?>" class="navbar-brand"><?php echo SITE_NAME; ?></a>

        <button class="navbar-toggler" data-toggle="collapse" data-target="#menu">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="menu">

            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-2">
                    <a href="<?php echo URL_ROOT; ?>" class="nav-link">Home</a>
                </li>

                <li class="nav-item mx-2">
                    <a href="<?php echo URL_ROOT; ?>/movies/movies" class="nav-link">Movies</a>
                </li>

                <li class="nav-item mx-2">
                    <a href="<?php echo URL_ROOT; ?>/movies/series" class="nav-link">Tv-Series</a>
                </li>

                <li class="nav-item mx-2">
                    <a href="<?php echo URL_ROOT; ?>/movies/news" class="nav-link">News</a>
                </li>

                <li class="nav-item ml-2 mr-4">
                    <a href="<?php echo URL_ROOT; ?>/movies/about" class="nav-link">About</a>
                </li>

                <form action="" method="post" id="search-box">
                    <div class="input-group">

                        <input type="text" name="search" class="form-control bg-transparent" placeholder="Enter keywords here">
                        <span class="input-group-btn input-group-prepend">
                            <a href="" class="btn btn-secondary input-group-text bg-transparent" name="submit">
                                <i class="fa fa-search"></i>
                            </a>
                        </span>

                    </div>

                </form>


            </ul>

        </div>
    </div>

</nav>

