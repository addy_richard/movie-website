<div class="carousel slide mb-5" id="my-carousel" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#my-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#my-carousel" data-slide-to="1" class=""></li>
        <li data-target="#my-carousel" data-slide-to="2" class=""></li>
        <li data-target="#my-carousel" data-slide-to="3" class=""></li>
        <li data-target="#my-carousel" data-slide-to="4" class=""></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item carousel-img-1 active">
            <div class="container">
                <div class="carousel-caption text-left mb-5">
                    <h3><a href="#">Heading One</a></h3>
                    <p class="lead w-50">A asperiores autem,
                            commodi corporis dolores eaque enim eum minima nam perspiciatis provident
                            quisquam sunt vel veniam veritatis voluptatibus, voluptatum.
                            Ad, beatae cupiditate ea harum iste perspiciatis! Ducimus, labore libero.
                    </p>
                </div>
            </div>
        </div>


        <div class="carousel-item carousel-img-2">
            <div class="container">
                <div class="carousel-caption text-left mb-5">
                    <h3><a href="#">Heading Two</a></h3>
                    <p class="lead w-50">A asperiores autem,
                        commodi corporis dolores eaque enim eum minima nam perspiciatis provident
                        quisquam sunt vel veniam veritatis voluptatibus, voluptatum.
                        Ad, beatae cupiditate ea harum iste perspiciatis! Ducimus, labore libero.
                    </p>
                </div>
            </div>
        </div>


        <div class="carousel-item carousel-img-3">
            <div class="container">
                <div class="carousel-caption text-left mb-5">
                    <h3><a href="#">Heading Three</a></h3>
                    <p class="lead w-50">A asperiores autem,
                        commodi corporis dolores eaque enim eum minima nam perspiciatis provident
                        quisquam sunt vel veniam veritatis voluptatibus, voluptatum.
                        Ad, beatae cupiditate ea harum iste perspiciatis! Ducimus, labore libero.
                    </p>
                </div>
            </div>
        </div>


        <div class="carousel-item carousel-img-4">
            <div class="container">
                <div class="carousel-caption text-left mb-5">
                    <h3><a href="#">Heading Four</a></h3>
                    <p class="lead w-50">A asperiores autem,
                        commodi corporis dolores eaque enim eum minima nam perspiciatis provident
                        quisquam sunt vel veniam veritatis voluptatibus, voluptatum.
                        Ad, beatae cupiditate ea harum iste perspiciatis! Ducimus, labore libero.
                    </p>
                </div>
            </div>
        </div>


        <div class="carousel-item carousel-img-5">
            <div class="container">
                <div class="carousel-caption text-left mb-5">
                    <h3><a href="#">Heading Five</a></h3>
                    <p class="lead w-50">A asperiores autem,
                        commodi corporis dolores eaque enim eum minima nam perspiciatis provident
                        quisquam sunt vel veniam veritatis voluptatibus, voluptatum.
                        Ad, beatae cupiditate ea harum iste perspiciatis! Ducimus, labore libero.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>