<?php require APP_ROOT . '/views/inc/header.php'; ?>
<?php include APP_ROOT . '/views/inc/carousel.php'; ?>

<div class="container">

    <!-- MOVIES SECTION -->

        <h3 class="text-light mb-3 heading">Latest Movies</h3>

        <div class="row mb-5">


            <?php foreach ($data['movies'] as $movie) : ?>

            <div class="col-lg-2 col-md-4 col-sm-4 mb-3">
                <a href="#">

                    <div class="card">
                        <img src="<?php echo URL_ROOT; ?>/public/img/<?php echo $movie->image; ?>" class="card-img" alt="">

                        <div class="card-img-overlay">
                            <span class="badge badge-success float-right"><?php echo $movie->quality; ?></span>
                        </div>
                    </div>
                </a>

            </div>

             <?php endforeach; ?>

        </div>
    <!-- END OF MOVIES SECTION -->

    <!-- TV SERIES SECTION -->

    <h3 class="text-light mb-3 heading">Latest Series</h3>

    <div class="row mb-5">


        <?php foreach ($data['series'] as $series) : ?>

            <div class="col-lg-2 col-md-4 col-sm-4 mb-3">
                <a href="#">

                    <div class="card">
                        <img src="<?php echo URL_ROOT; ?>/public/img/<?php echo $series->image; ?>" class="card-img" alt="">

                        <div class="card-img-overlay">
                            <span class="badge badge-success float-right"><?php echo $series->quality; ?></span>
                        </div>
                    </div>
                </a>

            </div>

        <?php endforeach; ?>

    </div>

    <!-- END TV SERIES SECTION -->


    <!-- ANIMATION SECTION -->

    <h3 class="text-light mb-3 heading">Latest Animations</h3>


    <div class="row mb-5">


        <?php foreach ($data['animations'] as $animation) : ?>

            <div class="col-lg-2 col-md-4 col-sm-4 mb-3">
                <a href="#">

                    <div class="card">
                        <img src="<?php echo URL_ROOT; ?>/public/img/<?php echo $animation->image; ?>" class="card-img" alt="">

                        <div class="card-img-overlay">
                            <span class="badge badge-success float-right"><?php echo $animation->quality; ?></span>
                        </div>
                    </div>
                </a>

            </div>

        <?php endforeach; ?>

    </div>

    <!-- END OF ANIMATION SECTION -->

</div>

<?php require APP_ROOT . '/views/inc/footer.php'; ?>
