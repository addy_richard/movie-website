<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 11/6/2018
 * Time: 2:13 PM
 */

    //DB PARAMS
    define('DB_HOST','localhost');
    define('DB_USER','root');
    define('DB_PASSWORD','');
    define('DB_NAME','movie_app');


    // APP ROOT
    define('APP_ROOT', dirname(dirname(__FILE__)));

    // URL ROOT
    define('URL_ROOT','http://localhost/movieApp');

    // SITE NAME
    define('SITE_NAME','Watcher');

    //APP VERSION
    define('APP_VERSION','1.0.0');