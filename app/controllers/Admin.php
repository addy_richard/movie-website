<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/13/2018
 * Time: 2:45 PM
 */

class Admin extends Controller
{
    public function __construct()
    {
        $this->adminModel = $this->model('AdminModel');
    }

    public function register(){

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){

            // PROCESS FORM
            $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

            $data = [
                'name' => trim($_POST['name']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'confirm_password' => trim($_POST['confirm_password']),
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''

            ];

            if (empty($data['name'])){
                $data['name_err'] = 'Please enter name';
            }
            if (empty($data['email'])){
                $data['email_err'] = 'Please enter email';
            }else{
                if($this->adminModel->findAdminByEmail($data['email'])){
                    $data['email_err'] = 'Email already registered';
                }
            }
            if (empty($data['password'])){
                $data['password_err'] = 'Please enter password';
            }elseif(strlen($data['password']) < 6){
                $data['password_err'] = 'Password must be at least 6 characters';
            }

            if (empty($data['confirm_password'])){
                $data['confirm_password_err'] = 'Please confirm password';
            }else{
                if ($data['confirm_password'] != $data['password']){
                    $data['confirm_password_err'] = 'Passwords do not match';
                }
            }

            // CHECK FOR NO ERROR
            if (empty($data['name_err']) && empty($data['email_err']) &&
                empty($data['password_err']) && empty($data['confirm_password_err'])){

                if($this->adminModel->registerAdmin($data)){
                    flash('register_success','You are registered, Please log in');
                    redirect('admin/login');
                }else{
                    die('Something went wrong. Please try again in few seconds');
                }
            }else{
                $this->view('admin/register',$data);
            }
        }else{
            $data = [
                'name' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''
            ];
            $this->view('admin/register',$data);
        }

    }

    public function login(){

        //CHECK FOR POST
        if ($_SERVER['REQUEST_METHOD'] == "POST"){
            //PROCESS FORM

            $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

            $data = [

                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_error' => '',
                'password_error' => '',
            ];

            if (empty($data['email'])){
                $data['email_error'] = 'Please enter email';
            }else{
                if (!$this->adminModel->findAdminByEmail($data['email'])){
                    $data['email_error'] = 'No Admin found';
                }
            }

            if (empty($data['password'])){
                $data['password_error'] = 'Please enter password';
            }



            if (empty($data['email_error']) && empty($data['password_error'])){
                //Check and set logged in user

                $loggedInAdmin = $this->adminModel->login($data['email'],$data['password']);


                if ($loggedInAdmin){
                    // CREATE USER SESSION
                    $this->createAdminSession($loggedInAdmin);

                }else{
                    $data['password_error'] = 'Password incorrect';

                    $this->view('admin/login',$data);
                }

            }else{
                //LOAD VIEW WITH DATA
                $this->view('admin/login',$data);
            }

        }else{
            //Init Data
            $data = [
                'email' => '',
                'password' => '',
                'email_error' => '',
                'password_error' => '',
            ];

            //LOAD VIEW
            $this->view('admin/login',$data);
        }
    }

    public function createAdminSession($admin){
        $_SESSION['admin_id'] = $admin->admin_id;
        $_SESSION['admin_name'] = $admin->name;
        $_SESSION['admin_email'] = $admin->email;

        redirect('admin');

    }

    public function index(){

        $allMovies = $this->adminModel->getAllMoviesAndSeries();
        $allAdmins = $this->adminModel->getAllAdmins();
        $movieCount = $this->adminModel->getMoviesCount();
        $adminCount = $this->adminModel->getAdminCount();

        $data = [
            'allMovies' => $allMovies,
            'movieCount' => $movieCount,
            'allAdmins' => $allAdmins,
            'adminCount' => $adminCount
        ];

        if (isset($_SESSION['admin_id'])){
            $this->view('admin/index',$data);
        }else{
            redirect('admin/login');
        }

    }

    public function logout(){
        unset($_SESSION['admin_id']);
        unset($_SESSION['admin_email']);
        unset($_SESSION['admin_name']);

        session_destroy();

        redirect('admin/login');
    }


}