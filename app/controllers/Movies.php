<?php
/**
 * Created by PhpStorm.
 * User: BalLack
 * Date: 12/12/2018
 * Time: 4:30 PM
 */

class Movies extends Controller
{
    public function __construct()
    {
        $this->moviesModel = $this->model('Movie');
    }

    public function index(){

        $movies = $this->moviesModel->getMovies();
        $series = $this->moviesModel->getSeries();
        $animations = $this->moviesModel->getAnimations();
        $data = [
            'movies' => $movies,
            'series' => $series,
            'animations' => $animations
        ];
        $this->view('movies/index',$data);
    }

    public function about(){

        $data = [
            'title' => 'Watcher',
            'description' => 'Movie App built on PHP MVC Framework',
            'version' => APP_VERSION
        ];

        $this->view('movies/about',$data);
    }

    public function news(){

    }

    public function series(){

    }

    public function movies(){

    }

}