-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2019 at 01:22 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movie_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `email`, `password`, `created_at`) VALUES
(2, 'ballack studios', 'ballackstudios@gmail.com', '$argon2i$v=19$m=1024,t=2,p=2$WTlOWmYvc0ZrbGEzVkFlaQ$LU6v1Qg/CaMVyiAX4DMDAXDy0jdakIpUIBLOcZAL4hQ', '2018-12-15 19:40:55'),
(3, 'john doe', 'jdoe@gmail.com', '$argon2i$v=19$m=1024,t=2,p=2$UDdBNGFtdWJlRzdRSVJJQw$MChko/ByzYYlg91Pf8HlgaL2O/XY+FWsRBnFL6sL9mY', '2018-12-16 01:01:39');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `plot` text NOT NULL,
  `year` year(4) NOT NULL,
  `quality` varchar(255) NOT NULL,
  `rating` varchar(10) NOT NULL,
  `image` varchar(255) NOT NULL,
  `tags` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `title`, `genre`, `plot`, `year`, `quality`, `rating`, `image`, `tags`, `type`, `country`) VALUES
(1, 'iron man 3', 'action , Sci-Fi , thriller', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, impedit, officia. At beatae culpa cum delectus ea, eius est fuga ipsa nam non omnis perferendis quae, quia reprehenderit, similique veniam.', 2013, 'HD', '7.6', 'ironman.jpg', 'ironman, tony stack , avengers , mavel , mcu', 'movie', ''),
(2, 'olympus has fallen', 'action , drama', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, impedit, officia. At beatae culpa cum delectus ea, eius est fuga ipsa nam non omnis perferendis quae, quia reprehenderit, similique veniam.', 2014, 'HD', '6.5', 'olympus.jpg', 'olympus, fallen , army , shooting', 'movie', ''),
(3, 'Wolverine', 'action , sci-fi , drama', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At autem beatae consequatur consequuntur dolore ducimus error eum expedita fugit, in iure porro quae quo rerum saepe sequi similique vero voluptates!', 2015, 'HD', '7.8', 'wolverine.jpg', 'x-men , logan , mutant', 'movie', ''),
(4, 'shadow hunters', 'action , adventure , sci-fi , horror,drama', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur aut consequuntur debitis fugiat ipsam magni maiores maxime molestiae nisi omnis provident quaerat quia ratione, reprehenderit rerum tempora velit vero. Eius.', 2016, 'HD', '6.8', 'shadowhunters.jpg', 'shadow hunters, motal instrument , jase , angel ,demons', 'series', ''),
(5, 'venom', 'action , horror , comedy , sci-fi', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2018, 'HD', '7.2', 'venom.jpg', '', 'movie', 'usa'),
(6, 'aquaman', 'action , sci-fi , horror', 'Aquaman finds himself caught between a surface world that ravages the sea and the underwater Atlanteans who are ready to revolt.', 2018, 'CAM', '8.2', 'aquaman.jpg', '', 'movie', ''),
(7, 'Incredibles 2', 'animation , action', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2018, 'HD', '8.6', 'incredibles2.jpg', '', 'animation', ''),
(8, 'small foot', 'animation , comedy', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2018, 'HD', '8.2', 'smallfoot.jpg', '', 'animation', ''),
(9, 'Epic', 'animation , action ,adventure ,sci-fi', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2013, 'HD', '7.9', 'epic.jpg', '', 'animation', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
